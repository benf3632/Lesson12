#include "MessagesSender.h"

MessagesSender::MessagesSender()
{
	if (!_outputFile.is_open())
	{
		_outputFile.open("output.txt", ios::out); //opens output file
	}
}


MessagesSender::~MessagesSender()
{
	if (_outputFile.is_open())
	{
		_outputFile.close(); //cloese output file
	}
}

/*
Menu for signing in or out
*/
void MessagesSender::menu()
{
	int op;
	bool stop = false;
	while (!stop)
	{
		do {
			system("cls");
			cout << "Welcome to Messages sender" << endl;
			cout << "1. Sign-in" << endl;
			cout << "2. Sign-out" << endl;
			cout << "3. Connected Users" << endl;
			cout << "4. exit" << endl;
			cin >> op;
		} while (op < SIGNIN || op > EXIT);

		switch (op)
		{
		case SIGNIN:
			signin();
			break;
		case SIGNOUT:
			signout();
			break;
		case USERS:
			printUsers();
			break;
		case EXIT:
			stop = true;
			break;
		}
	}
}

/*
Signs in a user
*/
void MessagesSender::signin()
{
	string user;
	system("cls");
	cout << "Enter user name" << endl;
	cin >> user;
	if (!_users.count(user)) //if user is not connected
	{
		_users.insert(user);
	}
	else
	{
		cout << "User name already exists" << endl;
		system("pause");
	}
}

/*
Sign outs a user
*/
void MessagesSender::signout()
{
	string user;
	system("cls");
	cout << "Enter user name" << endl;
	cin >> user;
	if (_users.count(user)) //if user is connected
	{
		_users.erase(user);
	}
	else
	{
		cout << "User name does'nt exists" << endl;
		system("pause");
	}
}

/*
Prints connected users
*/
void MessagesSender::printUsers()
{
	system("cls");
	for (auto it : _users)
	{
		cout << it << endl;
	}
	system("pause");
}

/*
Gets from the data.txt the messages and updates to the message queue
*/
void MessagesSender::updateMsgQueue()
{
	fstream file;
	string line;
	
	while (true)
	{
		if (!file.is_open())
		{
			file.open("data.txt"); //opens the data file
		}
		
		if (file.is_open())
		{
			unique_lock<mutex> lck(_muMsgQueue); //locks the msg queue 
			while (getline(file, line)) //runs until the data gatherd all data
			{
				_msgQueue.push_back(line);

			}
			lck.unlock(); //unlocks msg queue
			_cond.notify_all(); //wakes the thread for writing

			//clears the content of the data
			file.close();
			file.open("data.txt", ifstream::out | ifstream::trunc);
			file.close();
		}
		this_thread::sleep_for(chrono::seconds(60)); //sleeps for 60 sec
	}
}
/*
Writes to the output file the messages on the queue messages
*/
void MessagesSender::sendMsg()
{
	while (true)
	{
		unique_lock<mutex> lck_queue(_muMsgQueue); //locks the msg queue
		_cond.wait(lck_queue, [&]() {return !_msgQueue.empty(); }); //waits to wake up by another thread
		while (!_msgQueue.empty())
		{
			unique_lock<mutex> lck_users(_muUsers); //locks users
			if (!_users.empty()) 
			{
				for (auto it2 : _users)
				{
					_outputFile << it2 << ": " << _msgQueue[0] << endl; //writes to the file for every user in the user list
				}
			}
			else
			{
				_outputFile << _msgQueue[0] << endl; //if no user exists writes withou a user
			}
			lck_users.unlock(); //unlocks the user list
			_msgQueue.erase(_msgQueue.begin()); //delete the msg on queue and advances to the next one
		}
		lck_queue.unlock(); //unlocks msg queue
	}
}