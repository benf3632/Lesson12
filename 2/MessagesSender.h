#pragma once
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <mutex>
#include <thread>

using namespace std;

enum op { SIGNIN=1,SIGNOUT,USERS,EXIT };

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	void menu();
	void updateMsgQueue();
	void sendMsg();

private:
	set<string> _users;
	vector<string> _msgQueue;
	mutex _muUsers;
	mutex _muMsgQueue;
	condition_variable _cond;
	fstream _outputFile;
	void signin();
	void signout();
	void printUsers();

};

