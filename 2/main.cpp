#include "MessagesSender.h"


int main()
{
	MessagesSender msg;
	thread c(&MessagesSender::menu, &msg);
	thread a(&MessagesSender::updateMsgQueue, &msg);
	thread b(&MessagesSender::sendMsg, &msg);
	a.detach();
	b.detach();
	c.join();
	system("pause");
	return 0;
}